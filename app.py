from flask import Flask, render_template, request

from flask import Flask, request
import os
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
#from keras.applications.vgg16 import VGG16
from keras.applications.resnet50 import ResNet50

app = Flask(__name__)
model = ResNet50()

UPLOAD_FOLDER = 'uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return 'No file part', 400
    file = request.files['file']
    if file.filename == '':
        return 'No selected file', 400
    if file:
        filename = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
        file.stream.seek(0)
        file.save(filename)
        file.stream.seek(0)
        image_path = "./uploads/" + filename

        image = load_img(filename, target_size=(224, 224))
        image = img_to_array(image)
        image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
        image = preprocess_input(image)
        yhat = model.predict(image)
        label = decode_predictions(yhat)
        label = label[0][0]

        classification = '%s (%.2f%%)' % (label[1], label[2]*100)
        print(classification)  
        dict = {}
        dict["filename"]=filename
        dict["classification"]=classification
        dict["id"]=0
        # Renvoyer une confirmation que le fichier a été sauvegardé
        # return f"File saved to {filename} Classification {classification} Oussama", 200
        # {filename:filename , classification : classification , name : "oussama"}   
        return dict

if __name__ == '__main__':
    app.run()

